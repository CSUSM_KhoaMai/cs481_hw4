﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible (false)]
    public partial class MainPage : ContentPage

    {
        ObservableCollection<app_quote> app_quoteCollection;
        //Initilize app title and image
        public class app_quote
        {
            public string Title { get; set; }
            public string Img { get; set; }
        }

        public MainPage()
        {
            InitializeComponent();
            app_quoteCollection = new ObservableCollection<app_quote>() {
                //Add info into each of the item
                new app_quote()
                {
                    Title="Cake Quotes",
                    Img="cake.jpg",
                },
                new app_quote()
                {
                    Title="Water Quotes",
                    Img="water.jpg",
                },
                new app_quote()
                {
                    Title="Meat Quotes",
                    Img="meat.jpg",
                },
                new app_quote()
                {
                    Title="Beer Quotes",
                    Img="beer.jpg",
                },
            };
            CS481_HW4.ItemsSource = app_quoteCollection;

        }

        //Show new random quote everytime pullrefresh action
        void Handle_Refreshing(System.Object sender, System.EventArgs e)
        {
            CS481_HW4.IsRefreshing = false;
            base.OnAppearing();
            Random rnd = new Random();
            int rInt = rnd.Next(0, 3);

            List<string> birdQuotes = new List<string>(new string[] { "You’re off to great places, today is your day. Your mountain is waiting, so get on your way.",
                "You always pass failure on the way to success.",
                "No one is perfect - that’s why pencils have erasers." });
            dailyquote.Text = birdQuotes[rInt];

        }


        private async void OnItemSelected(Object sender, ItemTappedEventArgs e)
        {
            //currently, I use item click position to distinguise what is clicked
            Console.WriteLine("the E is: " + e.ItemIndex);
            if(e.ItemIndex == 0 )
                await Navigation.PushAsync(new CakePage());
            if (e.ItemIndex == 1)
                await Navigation.PushAsync(new WaterPage());
            if (e.ItemIndex == 2)
                await Navigation.PushAsync(new MeatPage());
            if (e.ItemIndex == 3)
                await Navigation.PushAsync(new BeerPage());

        }

    }
}
