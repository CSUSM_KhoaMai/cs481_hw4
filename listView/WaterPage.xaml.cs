﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WaterPage : ContentPage
	{
		public WaterPage()
		{
			InitializeComponent();
		}
		//create a list of string, then generate random number, then retrieve the string by random slot => random quotes.
		protected override void OnAppearing()
		{
			base.OnAppearing();
			Random rnd = new Random();
			int rInt = rnd.Next(0, 3);

			List<string> birdQuotes = new List<string>(new string[] { "This “water quotes” collection will inspire you to keep moving towards your goals and dreams no matter what obstacles stand in your path.",
				"Water is vital for all forms of life. Although it provides no calories or organic nutrients, water plays an important role in our world. It is the centre piece of all life.",
				"So when life throws obstacles in your way, be like water and either cut through them or form a new path that will lead you to your goals and dreams." });
			RandBirdText.Text = birdQuotes[rInt];

		}
		//Change the color of the bar which has the back button color. Reason: for cosmetic. Can be develop further in the future.
		protected override void OnDisappearing()
		{
			//base.OnDisappearing();
			//((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Green;

		}
	}
}