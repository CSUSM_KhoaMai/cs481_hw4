﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BeerPage : ContentPage
	{
		public BeerPage()
		{
			InitializeComponent();
		}
		//create a list of string, then generate random number, then retrieve the string by random slot => random quotes.
		protected override void OnAppearing()
		{
			base.OnAppearing();
			Random rnd = new Random();
			int rInt = rnd.Next(0, 3);

			List<string> birdQuotes = new List<string>(new string[] { "Beer is proof that God loves us and wants us to be happy.",
				"All right, brain. You don’t like me, and I don’t like you, but let’s just do this and I can get back to killing you with beer.",
				"I feel sorry for people who don’t drink. When they wake up in the morning, that’s as good as they’re going to feel all day." });
			RandBirdText.Text = birdQuotes[rInt];

		}
		//Change the color of the bar which has the back button color. Reason: for cosmetic. Can be develop further in the future.
		protected override void OnDisappearing()
		{
			//base.OnDisappearing();
			//((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Green;

		}
	}
}