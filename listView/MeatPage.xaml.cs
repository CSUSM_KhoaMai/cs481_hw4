﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MeatPage : ContentPage
	{
		public MeatPage()
		{
			InitializeComponent();
		}
		//create a list of string, then generate random number, then retrieve the string by random slot => random quotes.
		protected override void OnAppearing()
		{
			base.OnAppearing();
			Random rnd = new Random();
			int rInt = rnd.Next(0, 3);

			List<string> birdQuotes = new List<string>(new string[] { "If you don't eat yer meat, you can't have any pudding.  How can you have any pudding if you don't eat yer meat?",
				"Meat Meat! We are going to eat some meat; and what meat!  Real game! Still no bread, though.",
				"The poor man must walk to get meat for his stomach, the rich man to get a stomach to his meat." });
			RandBirdText.Text = birdQuotes[rInt];

		}
		//Change the color of the bar which has the back button color. Reason: for cosmetic. Can be develop further in the future.
		protected override void OnDisappearing()
		{
			//base.OnDisappearing();
			//((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Green;

		}
	}
}