﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CakePage : ContentPage
	{
		public CakePage()
		{
			InitializeComponent();
		}
		//create a list of string, then generate random number, then retrieve the string by random slot => random quotes.
		protected override void OnAppearing()
		{
			base.OnAppearing();
			Random rnd = new Random();
			int rInt = rnd.Next(0, 3);

			List<string> birdQuotes = new List<string>(new string[] { "Take the broken pieces of your life, bake a master cake out of it. Don't stand still like a lake; keep flowing like a stream!",
				"Cakes are special. Every birthday, every celebration ends with something sweet, a cake, and people remember. It's all about the memories.",
				"Happiness is like a cake: have too much of it and you get sick of it." });
			RandBirdText.Text = birdQuotes[rInt];

		}
		//Change the color of the bar which has the back button color. Reason: for cosmetic. Can be develop further in the future.
		protected override void OnDisappearing()
		{
			//base.OnDisappearing();
			//((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.Green;

		}
	}
}